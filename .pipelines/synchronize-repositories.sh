#!/bin/sh

SOURCE_REPOSITORY_NAME=blazor-graph.git
SOURCE_URL=git@bitbucket.org:lrottach/$SOURCE_REPOSITORY_NAME
TARGET_REPOSITORY_NAME=temp-testing
TARGET_URL=git@ssh.dev.azure.com:v3/baggenstos/lrottach/$TARGET_REPOSITORY_NAME

echo "Current path is"
pwd

echo "Current directory"
ls

eval "$(ssh-agent)"

echo "Enabling source ssh key"
chmod 600 .ssh/source
# Only for local debugging
# git config --local core.sshCommand "/usr/bin/ssh -i .ssh/source"
ssh-add .ssh/source

if [ "$?" != "0" ]; then
echo "Failed to load source ssh key"
exit 1
fi

echo "Deleting any old source that may exist"
rm -rf $SOURCE_REPOSITORY_NAME

echo "Checking out the source repository"
git clone --bare $SOURCE_URL

if [ "$?" != "0" ]; then
echo "Failed to clone source"
exit 1
fi

echo "Entering the checked out repository"
cd $SOURCE_REPOSITORY_NAME

echo "Downloading the source repository"
git fetch origin --tags

if [ "$?" != "0" ]; then
echo "Failed to fetch origin"
exit 1
fi

echo "Enabling the target ssh key"
chmod 600 ../.ssh/target
# Only for local debugging
# git config --local core.sshCommand "/usr/bin/ssh -i ../.ssh/target"
ssh-add ../.ssh/target

if [ "$?" != "0" ]; then
echo "Failed to load target ssh key"
exit 1
fi

echo "Adding the target as mirror remote"
git remote add --mirror=fetch target $TARGET_URL

if [ "$?" != "0" ]; then
echo "Failed to add target as remote: $TARGET_URL"
exit 1
fi

if [ "$?" != "0" ]; then
echo "Failed to fetch target"
exit 1
fi

echo "Copying all data from the source to the target repository"
git push target --all
git push target --tags

if [ "$?" != "0" ]; then
echo "Failed to push to target"
exit 1
fi

cd ..

echo "Deleting any old source that may exist"
rm -rf $SOURCE_REPOSITORY_NAME

# Only for local debugging
# git config --local core.sshCommand "/usr/bin/ssh"
